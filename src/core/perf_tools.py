import time
from statistics import geometric_mean
from src.core.btree import BTree

NUMBER_OF_TRIALS = 10


def _do_performance_trial(function, *args) -> float:
    start_time = time.perf_counter()
    function(args)
    end_time = time.perf_counter()
    return end_time - start_time


def get_performance(function, *args) -> float:
    time_measurements: list[float] = []

    for i in range(NUMBER_OF_TRIALS):
        tree = BTree()
        time_taken = _do_performance_trial(function, tree, args)
        time_measurements.append(time_taken)

    return geometric_mean(time_measurements)


def get_performance_preserve_tree(function, *args) -> float:
    time_measurements: list[float] = []

    for i in range(NUMBER_OF_TRIALS):
        try:
            tree: BTree = args[0][i]
        except TypeError:
            tree: BTree = args[0]

        time_taken = _do_performance_trial(function, tree, args)
        time_measurements.append(time_taken)

    return geometric_mean(time_measurements)
