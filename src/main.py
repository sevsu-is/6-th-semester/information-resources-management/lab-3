import matplotlib.pyplot as plt

import ui
from core.btree import BTree
from core.perf_tools import (get_performance, get_performance_preserve_tree)
from people.tree_helper import (read_records_from_csv, fill_tree_with_records)
from people.people import Person


def main():
    tree = BTree()
    window = ui.render_main_window()

    ui.render_batch_ins_button(window, measure_ins_perf)
    ui.render_display_tree_button(window, tree)

    search_field = ui.render_search_field(window)
    ui.render_search_test_btn(window, search_field.get, measure_find_perf)

    remove_field = ui.render_remove_field(window)
    ui.render_removal_test_button(window, remove_field.get, measure_rem_perf)
    ui.render_remove_button(window, remove_field.get, lambda name: tree.remove(name))
    ui.render_add_button(window, tree, handle_manual_insertion)

    window.mainloop()


def measure_ins_perf():
    records = read_records_from_csv('people.csv')
    number_of_elements, elapsed_time = measure_ins_performance(records)
    plt.plot(number_of_elements, elapsed_time, label='Insertion performance')
    plt.xlabel('Number of elements')
    plt.ylabel('Time taken')
    plt.show()


def measure_ins_performance(records: list[list[str]]) -> (list[int], list[float]):
    number_of_elements: list[int] = [10, 50, 100, 250, 500, 1_000, 3_500, 5_500, 7_500, 10_000]
    elapsed_time: list[float] = []

    for measurement in number_of_elements:
        time_taken = get_performance(fill_tree_function, records[:measurement])
        elapsed_time.append(time_taken)

    return number_of_elements, elapsed_time


def fill_tree_function(*args) -> None:
    tree: BTree = args[0][0]
    records = args[0][1][0]
    fill_tree_with_records(tree, records)


def measure_find_perf(key: str):
    records = read_records_from_csv('people.csv')
    number_of_elements, elapsed_time = measure_search_performance(records, key)

    plt.plot(number_of_elements, elapsed_time, label='Search performance')
    plt.xlabel('Number of elements')
    plt.ylabel('Time taken to find')
    plt.show()


def measure_rem_perf(key: str):
    records = read_records_from_csv('people.csv')
    number_of_elements, elapsed_time = measure_remove_performance(records, key)

    plt.plot(number_of_elements, elapsed_time, label='Search performance')
    plt.xlabel('Number of elements')
    plt.ylabel('Time taken to find')
    plt.show()


def measure_search_performance(records: list[list[str]], key: str) -> (list[int], list[float]):
    trees: list[BTree] = []
    number_of_elements: list[int] = [10, 50, 100, 250, 500, 1_000, 3_500, 5_500, 7_500, 10_000]
    elapsed_time: list[float] = []

    for measurement in number_of_elements:
        tree: BTree = BTree()
        fill_tree_with_records(tree, records[:measurement])
        trees.append(tree)

    for tree in trees:
        time_taken = get_performance_preserve_tree(find_element_function, tree, key)
        elapsed_time.append(time_taken)

    return number_of_elements, elapsed_time


def measure_remove_performance(records: list[list[str]], key: str) -> (list[int], list[float]):
    trees: list[list[BTree]] = []
    number_of_elements: list[int] = [10, 50, 100, 250, 500, 1_000, 3_500, 5_500, 7_500, 10_000]
    elapsed_time: list[float] = []

    i = 0
    for measurement in number_of_elements:
        trees.append([])
        for trial in range(10):
            tree: BTree = BTree()
            fill_tree_with_records(tree, records[:measurement])
            trees[i].append(tree)
        i = i + 1

    for tree in trees:
        time_taken = get_performance_preserve_tree(remove_element_function, tree, key)
        elapsed_time.append(time_taken)

    return number_of_elements, elapsed_time


def find_element_function(*args):
    tree: BTree = args[0][1][0]
    element_key: str = args[0][1][1]
    found_element = tree.search(element_key)
    return found_element


def remove_element_function(*args):
    tree: BTree = args[0][0]
    element_key: str = args[0][1][1]
    tree.remove(element_key)


def handle_manual_insertion(tree: BTree, name: str, email: str, profession: str):
    person = Person(name, email, profession)
    tree.insert(person)


if __name__ == '__main__':
    main()
