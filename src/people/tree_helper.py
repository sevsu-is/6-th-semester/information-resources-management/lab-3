import csv
from src.core.btree import BTree
from src.people.people import Person


def read_records_from_csv(filename: str) -> list[list[str]]:
    lines: list[list[str]] = []
    
    with open(filename, 'r') as file:
        csv_reader = csv.reader(file)
        next(csv_reader)

        for i, row in enumerate(csv_reader):
            lines.append(row)

    return lines
                

def fill_tree_with_records(tree: BTree, records) -> None:
    for record in records:
        name, email, profession = record
        person = Person(name, email, profession)
        tree.insert(person)
