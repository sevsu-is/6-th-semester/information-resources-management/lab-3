class Person:
    def __init__(self, name: str, email: str, profession: str):
        self.name = name
        self.email = email
        self.profession = profession

    def __str__(self):
        return f'{self.name} ({self.email}) - {self.profession}'

    def __gt__(self, other):
        try:
            return self.name > other.name
        except AttributeError:
            return self.name > other

    def __lt__(self, other):
        try:
            return self.name < other.name
        except AttributeError:
            return self.name < other

    def __eq__(self, other):
        try:
            return self.name == other.name
        except AttributeError:
            return self.name == other
