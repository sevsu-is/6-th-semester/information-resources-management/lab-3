import tkinter as tk
from tkinter import ttk
from core.btree import BTree


def render_main_window():
    window = tk.Tk()
    window.geometry('450x300')
    window.title("B-Tree Inspector")
    label = tk.Label(
        window,
        text="Welcome to B-Tree inspector! This program allows you to test and see " +
             "the performance benefits of using B-Trees for yourself",
        wraplength=350
    )
    label.grid(column=0, row=0, columnspan=2, padx=5, pady=2)
    return window


def render_batch_ins_button(window: tk.Tk, event_handler):
    ins_test_btn = tk.Button(window, text='Batch insertion test', command=event_handler, width=15)
    ins_test_btn.grid(column=0, row=1, columnspan=2, sticky='W', padx=5, pady=2)


def render_search_field(window: tk.Tk):
    search_field = tk.Entry(window, width=35)
    search_field.grid(row=3, column=0, sticky='W', padx=2, pady=2)
    search_field.insert(0, 'Johna Alice')
    return search_field


def render_search_test_btn(window: tk.Tk, get_val_fn, event_handler):
    search_test_btn = tk.Button(window, text='Search test',
                                command=lambda: event_handler(get_val_fn()), width=15)
    search_test_btn.grid(row=3, column=1, sticky='W')


def render_remove_field(window: tk.Tk):
    remove_field = tk.Entry(window, width=35)
    remove_field.grid(row=4, column=0, sticky='W', padx=2, pady=2)
    remove_field.insert(0, 'Sherrie Vernier')

    return remove_field


def render_removal_test_button(window: tk.Tk, get_value_fn, event_handler):
    search_test_btn = tk.Button(window, text='Removal test',
                                command=lambda: event_handler(get_value_fn()), width=15)
    search_test_btn.grid(row=4, column=1, sticky='W')


def render_remove_button(window: tk.Tk, get_value_fn, event_handler):
    search_test_btn = tk.Button(window, text='Remove',
                                command=lambda: event_handler(get_value_fn()), width=15)
    search_test_btn.grid(row=5, column=0, sticky='W')


def render_add_button(window: tk.Tk, tree: BTree, insertion_handler):
    add_record_btn = tk.Button(window, text='Add record',
                               command=lambda: render_insertion_form(tree, insertion_handler), width=15)
    add_record_btn.grid(row=5, column=1, sticky='W', padx=5, pady=2)


def render_insertion_form(tree: BTree, insertion_handler):
    add_record_window = tk.Tk()
    add_record_window.geometry('400x300')
    add_record_window.title("Manually adding record")

    name_label = tk.Label(add_record_window, text='Name')
    name_label.pack()
    name_field = tk.Entry(add_record_window)
    name_field.pack()

    email_label = tk.Label(add_record_window, text='Email')
    email_label.pack()
    email_field = tk.Entry(add_record_window)
    email_field.pack()

    profession_label = tk.Label(add_record_window, text='Profession')
    profession_label.pack()
    profession_field = tk.Entry(add_record_window)
    profession_field.pack()

    add_btn = tk.Button(add_record_window, text='Add record',
                        command=lambda: insertion_handler(tree, name_field.get(), email_field.get(),
                                                          profession_field.get())
                        )
    add_btn.pack()


def render_display_tree_button(window: tk.Tk, tree: BTree):
    display_tree_btn = tk.Button(window, text='Display tree', command=lambda: render_display_tree(tree), width=15)
    display_tree_btn.grid(row=1, column=1, sticky='W', padx=5, pady=2)


def render_display_tree(tree: BTree):
    tree_window = tk.Tk()
    tree_window.title('Display tree')
    tree_window.geometry('800x600')
    tree_view = ttk.Treeview(tree_window, height=50)
    tree_view['columns'] = ('name', 'email', 'profession')

    tree_view.heading('#0', text='ID')
    tree_view.heading('name', text='Name')
    tree_view.heading('email', text='Email')
    tree_view.heading('profession', text='Profession')

    for item in tree:
        tree_view.insert('', 'end', text=item.name,
                         values=(item.name, item.email, item.profession))

    tree_view.pack(fill='both', expand=True)

